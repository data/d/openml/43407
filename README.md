# OpenML dataset: Advertising-Campaign-Analytics-Merkle-Sokrati

https://www.openml.org/d/43407

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Problem Statement
The following Data Analysis of Marketing Campaigns is a part of the Assignment for Data Science Intern at Merkle Sokrati.
Objectives of the Task

Carry out EDA and build ML model to evaluate the insights automatically.
Prepare a summary of your Analysis and put that into a professional looking deck.

Content
Marketing campaigns containing data from Oct19 to July20. This data is from Google and Facebook campaigns which shows the performance of different Age-groups for different dimensions.
All the key fields like Platform, Type, Medium, Sub Channel, Audience, Creative have already been mapped to the data.

Platform: Marketing platforms on which campaigns are running majorly: Google Ads and Facebook Ads.
Type: Type of campaign, In this data, only Google search and Facebook Conversion campaigns have been considered.
Medium: The way we are connecting to people in our Marketing campaigns either via some Keywords or Creatives.
Sub Channel: Subchannel is under Google Search which type of keywords have been targeted, In Facebook which on subchannel we are targeting.
Audience: Multiple Type of audiences are getting targeted in different campaigns and those have been encrypted as Audience 1,2,3.
Creative: This if for Facebook what type of Image/Video/Carousel we are using in our Ads.

Acknowledgements
Merkle Sokrati has been sending these assignments for data analyst, data science intern positions and not replying back after the submission. So why not democratize my work and save other's time.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43407) of an [OpenML dataset](https://www.openml.org/d/43407). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43407/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43407/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43407/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

